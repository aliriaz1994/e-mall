pwd
php bin/magento cache:clean
php bin/magento cache:flush
rm generated/* -r

rm pub/static/frontend/* -r

rm var/view_preprocessed/* -r

php bin/magento setup:upgrade
php bin/magento setup:di:compile

php bin/magento setup:static-content:deploy -f

#chmod 777 . -R

