<?php
namespace WeSupply\Toolbox\Plugin\Magento\Checkout\Block\Checkout;

class LayoutProcessor
{

    /**
     * @var \WeSupply\Toolbox\Helper\Data
     */
    private $helper;

    /**
     * LayoutProcessor constructor.
     * @param \WeSupply\Toolbox\Helper\Data $helper
     */
    public function __construct(
        \WeSupply\Toolbox\Helper\Data $helper
    )
    {
        $this->helper = $helper;
    }


    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param $result
     * @return mixed
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        $result
    ) {

        if (!$this->helper->getWeSupplyEnabled() ||  !$this->helper->getDeliveryEstimationsEnabled()) {

            if (isset($result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['config']['template'])) {
                if ($result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['config']['template'] == 'WeSupply_Toolbox/shipping') {
                    unset($result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['config']['template']);
                }

            }
        }
        return $result;

    }

}
