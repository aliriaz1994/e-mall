<?php
/**
 * @category    WeltPixel
 * @package     WeltPixel_SocialLogin
 * @copyright   Copyright (c) 2018 WeltPixel
 */

namespace WeltPixel\SocialLogin\Block;


/**
 * Class SocialLogin
 * @package WeltPixel\SocialLogin\Block
 */
class SocialLogin extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \WeltPixel\SocialLogin\Helper\Data
     */
    protected $slHelper;
    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $assetRepo;
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $_request;
    /**
     * @var array
     */
    protected $_socialMedia = [
        'fb' => 'https://www.facebook.com/dialog/oauth/?',
        'instagram' => 'https://api.instagram.com/oauth/authorize/?',
        'amazon' => 'https://www.amazon.com/ap/oa/?',
        'google' => 'https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&access_type=offline&include_granted_scopes=true&state=state_parameter_passthrough_value&',
        'twitter' => 'https://api.twitter.com/oauth/request_token',
        'linkedin' => 'https://www.linkedin.com/oauth/v2/authorization?',
        'paypal' => 'https://www.paypal.com/signin/authorize?'
    ];
    /**
     * @var \WeltPixel\SocialLogin\Model\Twitter
     */
    protected $twitterModel;

    /**
     * @var \WeltPixel\SocialLogin\Model\Paypal
     */
    protected $paypalModel;

    /**
     * @var \Magento\Framework\View\Page\Config
     */
    protected $page;

    /**
     * SocialLogin constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\View\Asset\Repository $assetRepo
     * @param \WeltPixel\SocialLogin\Helper\Data $slHelper
     * @param \WeltPixel\SocialLogin\Model\Twitter $twitterModel
     * @param \WeltPixel\SocialLogin\Model\Paypal $paypalModel
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Framework\View\Page\Config $page
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \WeltPixel\SocialLogin\Helper\Data $slHelper,
        \WeltPixel\SocialLogin\Model\Twitter $twitterModel,
        \WeltPixel\SocialLogin\Model\Paypal $paypalModel,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\View\Page\Config $page,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->slHelper = $slHelper;
        $this->assetRepo = $assetRepo;
        $this->_request = $request;
        $this->twitterModel = $twitterModel;
        $this->paypalModel = $paypalModel;
        if ($this->slHelper->isEnabled()) {
            $page->addBodyClass('wp-sl');
        }
    }

    /**
     * @return string|void
     */
    protected function _toHtml()
    {
            if (!$this->slHelper->isEnabled()) {
            return;
        }

        return parent::_toHtml();
    }

    /**
     * @return string
     */
    public function getSkipModules()
    {
        $skip = $this->slHelper->getSkipModulesReferer();
        return json_encode($skip);
    }

    /**
     * @return bool|string
     */
    protected function _isCheckoutPage() {
        $route      = $this->_request->getRouteName();
        $controller = $this->_request->getControllerName();
        if($route == 'checkout' && $controller == 'index') {
            return $this->getUrl('checkout/index/index', ['secure' => true]);
        } else {
            return false;
        }
    }

    /**
     * @return bool|string
     */
    protected function _isCartPage() {
        $route      = $this->_request->getRouteName();
        $controller = $this->_request->getControllerName();
        if($controller == 'cart' && $route == 'checkout') {
            return $this->getUrl('checkout/cart/index', ['secure' => true]);
        } else {
            return false;
        }
    }

    /**
     * @return bool|string
     */
    public function getCurrentPageRedirectUrl() {
        $url = false;
        if($cartUrl = $this->_isCartPage()) {
            return $cartUrl;
        } elseif($checkoutUrl = $this->_isCheckoutPage()) {
            return $checkoutUrl;
        } else {
            return $url;
        }
    }


}