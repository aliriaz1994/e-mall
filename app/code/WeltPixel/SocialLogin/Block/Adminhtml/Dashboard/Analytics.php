<?php
/**
 * @category    WeltPixel
 * @package     WeltPixel_{Module}
 * @copyright   Copyright (c) 2018 WeltPixel
 * @author      WeltPixel TEAM
 */


namespace WeltPixel\SocialLogin\Block\Adminhtml\Dashboard;


class Analytics extends \Magento\Backend\Block\Template
{

    /**
     * @var \WeltPixel\SocialLogin\Model\Analytics
     */
    protected $analitycs;

    /**
     * Analytics constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \WeltPixel\SocialLogin\Model\Analytics $analitycs
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \WeltPixel\SocialLogin\Model\Analytics $analitycs,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->analitycs = $analitycs;
    }

    /**
     * @return array
     */
    public function fetchAnalyticsData() {
        return $this->analitycs->getAnalyticsData();
    }

    public function fetchAnalyticsTotals() {
        return $this->analitycs->getAnalyticsTotals();
    }

}