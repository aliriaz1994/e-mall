<?php
/**
 * @category    WeltPixel
 * @package     WeltPixel_SocialLogin
 * @copyright   Copyright (c) 2018 WeltPixel
 * @author      WeltPixel TEAM
 */

namespace WeltPixel\SocialLogin\Ui\DataProvider;

use Magento\Ui\DataProvider\AbstractDataProvider;
use WeltPixel\SocialLogin\Model\SocialloginFactory;

/**
 * Class DataProvider
 * @package WeltPixel\SocialLogin\Ui\DataProvider
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * @var SocialloginFactory
     */
    protected $socialloginFactory;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        SocialloginFactory $socialloginFactory,
        array $meta = [],
        array $data = [])
    {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->socialloginFactory = $socialloginFactory;
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getCollection()
    {
        $collection = $this->socialloginFactory->create()->getCollection()
            ->join(['ce' => 'customer_entity'], 'ce.entity_id = main_table.customer_id', ['firstname','lastname','email']);
        return $collection;

    }

    /**
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        return $this->getCollection()->toArray();
    }
}