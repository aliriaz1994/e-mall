<?php
/**
 * @category    WeltPixel
 * @package     WeltPixel_{Module}
 * @copyright   Copyright (c) 2018 WeltPixel
 * @author      WeltPixel TEAM
 */

namespace WeltPixel\SocialLogin\Model;

/**
 * Class Analytics
 * @package WeltPixel\SocialLogin\Model
 */
class Analytics
{
    /**
     * @var array
     */
    protected $_socialMedia = [
        'fb' => 'Facebook',
        'amazon' => 'Amazon',
        'google' => 'Google',
        'instagram' => 'Instagram',
        'twitter' => 'Twitter',
        'linkedin' => 'LinkedIn',
        'paypal' => 'PayPal',
        'default' => 'Email & Password',
        'guest' => 'Guest'
    ];
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_orderCollectionFactory;

    /**
     * @var OrderUserFactory
     */
    protected $orderUserFactory;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    private $objectFactory;

    /**
     * @var SocialloginFactory
     */
    protected $socialLoginModel;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    protected $_customerFactory;

    /**
     * @var
     */
    protected $_dataObjectArr = [];

    /**
     * @var
     */
    protected $_model;

    /**
     * @var
     */
    protected $_type;

    /**
     * @var
     */
    protected $_excludedSocialCustomerIds;

    /**
     * @var
     */
    protected $_excludedOrderIds;

    /**
     * @var
     */
    protected $_orderIdsByType;

    /**
     * @var
     */
    protected $_orders;

    /**
     * @var
     */
    protected $_ordersFiltered;

    /**
     * Analytics constructor.
     * @param \Magento\Framework\DataObjectFactory $objectFactory
     * @param SocialloginFactory $socialLoginModel
     */
    public function __construct(
        \Magento\Framework\DataObjectFactory $objectFactory,
        \WeltPixel\SocialLogin\Model\SocialloginFactory $socialLoginModel,
        \WeltPixel\SocialLogin\Model\OrderUserFactory $orderUserFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory
    )
    {
        $this->objectFactory = $objectFactory;
        $this->socialLoginModel = $socialLoginModel;
        $this->_customerFactory = $customerFactory;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->orderUserFactory = $orderUserFactory;
    }

    /**
     * @return array
     */
    public function getAnalyticsData() {
        return $this->_setDataOject();
    }

    /**
     * @return mixed
     */
    public function getAnalyticsTotals() {
        return $this->_setTotalDataObject();
    }

    /**
     * @return array
     */
    protected function _setDataOject() {

        $this->_model = $this->socialLoginModel->create();
        $this->_orderUserModel = $this->orderUserFactory->create();
        $this->_excludedSocialCustomerIds = $this->_model->getCustomerIdArr();
        $this->_excludedOrderIds = $this->_orderUserModel->getOrderIdsArr();

        foreach ($this->_socialMedia as $type => $label) {
            $this->_type = $type;
            $this->_orderIdsByType = $this->_orderUserModel->getOrderIdsByType($this->_type);
            $this->_setFilteredOrdersCollection();
            $this->_setAllOrdersCollection();
            $dataObject = $this->objectFactory->create();

            $userCount = ($this->_type == 'guest') ? '-' : $this->_countUsers();
            $userPercent = ($this->_type == 'guest') ? '-' : $this->_calculatePercent($this->_countAllCustomers(), $userCount);
            $userOrders = $this->_countOrders();
            $userOrdersPercent = $this->_calculatePercent($this->_countAllOrders(), $userOrders);
            $orderItems = $this->_countItems();
            $orderItemsPercent = $this->_calculatePercent($this->_countAllOrdersItems(), $orderItems);
            $revenue = $this->_getRevenue();
            $revenuePercent = $this->_calculatePercent($this->_getTotalRevenues(), $revenue);


            $dataObject->setUsersNo($userCount);
            $dataObject->setUsersPrecent($userPercent);
            $dataObject->setOrdersNo($userOrders);
            $dataObject->setOrdersPercent($userOrdersPercent);
            $dataObject->setItemsNo($orderItems);
            $dataObject->setItemsPercent($orderItemsPercent);
            $dataObject->setRevenue($revenue);
            $dataObject->setRevenuePercent($revenuePercent);

            $this->_dataObjectArr[$label] = $dataObject;
        }

        return $this->_dataObjectArr;
    }

    /**
     * @return \Magento\Framework\DataObject
     */
    protected function _setTotalDataObject() {
        $totalDataObject = $this->objectFactory->create();
        $totalDataObject->setTotalCustomers($this->_countAllCustomers());
        $totalDataObject->setTotalOrders($this->_countAllOrders());
        $totalDataObject->setTotalOrdersItems($this->_countAllOrdersItems());
        $totalDataObject->setTotalRevenue($this->_getTotalRevenues());

        return $totalDataObject;
    }

    /**
     * @return mixed
     */
    protected function _countUsers() {
        if($this->_type == 'default') {
            return $this->_countDefaultCustomers();
        }

        return $this->_model->countUsersByType($this->_type);
    }

    protected function _countDefaultCustomers() {

        $collection = $this->_customerFactory->create()
            ->addFieldToFilter(
                'entity_id', ['nin' => $this->_excludedSocialCustomerIds]
            );

        return $collection->getSize();
    }

    /**
     * @return mixed
     */
    protected function _countAllCustomers() {
        $collection = $this->_customerFactory->create();

        return $collection->getSize();
    }

    /**
     * @param $customerId
     * @return mixed
     */
    protected function _countOrders() {
        if($this->_type == 'default') {
            $orderIds = $this->_excludedOrderIds;
            $orders = $this->_orderCollectionFactory->create();
            if(!empty($orderIds)) {
                $orders->addFieldToFilter('entity_id',['nin' => $orderIds]);
            }
            $orders->addFieldToFilter('customer_id',['neq' => 'NULL']);
        } elseif($this->_type == 'guest') {
            $orders = $this->_orderCollectionFactory->create()->addFieldToFilter('customer_id',['null' => true]);
        } else {
            $orders = $this->_orderUserModel->getCollection()->addFieldToFilter('type', $this->_type);
        }

        return $orders->getSize();
    }


    /**
     * @return mixed
     */
    protected function _countAllOrders() {
        $orders = $this->_orderCollectionFactory->create();

        return $orders->getSize();
    }

    /**
     * @return mixed
     */
    protected function _setFilteredOrdersCollection() {

        if($this->_type == 'default') {
            $orderIds = $this->_excludedOrderIds;
            $orders = $this->_orderCollectionFactory->create();
            if(!empty($orderIds)) {
                $orders->addFieldToFilter('entity_id',['nin' => $orderIds]);
            }
            $orders->addFieldToFilter('customer_id',['neq' => 'NULL']);
                $this->_ordersFiltered = $orders;
        } elseif($this->_type == 'guest') {
            $this->_ordersFiltered  = $this->_orderCollectionFactory->create()
                ->addFieldToFilter('customer_id',['null' => true]);
        } else {
            $orderIdsByType = $this->_orderIdsByType;
            $this->_ordersFiltered = $this->_orderCollectionFactory->create()
                ->addFieldToFilter('entity_id',['in' => $orderIdsByType]);
        }
    }

    /**
     * @return \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    protected function _setAllOrdersCollection() {
        $this->_orders = $this->_orderCollectionFactory->create();
        return $this->_orders;
    }

    /**
     * @return int
     */
    protected function _countItems() {
        $items = 0;
        foreach($this->_ordersFiltered as $order) {
            $items += $order->getTotalItemCount();
        }

        return $items;
    }

    /**
     * @return int
     */
    protected function _countAllOrdersItems() {
        $items = 0;
        foreach($this->_orders as $order) {
            $items += $order->getTotalItemCount();
        }

        return $items;
    }

    /**
     * Calculation based on 'based_total_invoiced'
     * @return int
     */
    protected function _getRevenue() {
        $revenue = 0;
        foreach($this->_ordersFiltered as $order) {
            $revenue += $order->getBaseGrandTotal();
        }

        return $revenue;
    }

    /**
     * Calculation based on 'base_total_invoiced'
     * @return int
     */
    protected function _getTotalRevenues() {
        $revenue = 0;
        foreach($this->_orders as $order) {
            $revenue += $order->getBaseGrandTotal();
        }

        return $revenue;
    }

    /**
     * @param $total
     * @param $number
     * @return float
     */
    private function _calculatePercent($total, $number) {
        $prec = ($number * 100) / $total;

        return round($prec, 1);
    }


}